#Bibliotecas
import schedule
import requests
from lxml import html

#Dados
def evap():
  pagina = requests.get('https://online.unisc.br/tempo/home.seam')
  dados = html.fromstring(pagina.content)
  nome = dados.xpath('//*[@id="workspaces:j_idt52"]/div/span[1]/text()')

  print('NÍVEL DE EVAPOTRANSPIRAÇÃO ')
  print(nome[0])

#timer
schedule.every(10).seconds.do(evap)
while True:
  schedule.run_pending()